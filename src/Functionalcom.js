import React from 'react'

function Functionalcom(props) {
  return (
    <div>
        <h1>
            i am from functional component {props.name}
        </h1>
        <h2>iam from {props.batch}</h2>
       
    </div>

  )
}

export default Functionalcom
