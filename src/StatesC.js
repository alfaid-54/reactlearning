import React, { Component } from 'react'

export default class StatesC extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
        count:0
      }
    }

    handleIncrement = ()=>{
        // this.state.count = this.state.count+1
        this.setState({
            count:this.state.count+1
        })
    }
    handleDecrement = () =>{
      // this.state.count = this.state.count-1
      this.setState({
        count:this.state.count-1
      })
    }
  render() {
    return (
      <div>
        <h1>
            {this.state.count}

        </h1>
        <button onClick={this.handleIncrement}>Increment</button>
        <button onClick={this.handleDecrement}>Decrement</button>







      </div>
    )
  }
}
