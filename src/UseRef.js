import React, { useRef, useState } from 'react'

const UseRef = () => {
    const [name, setName] = useState("");
    const username = useRef();
    const useremail = useRef();
    const count = useRef(0);

    const handleFocus = () =>{
        // console.log(username.current.value);
        // console.log(useremail.current.value);
        (username.current.focus());

    
    }
   
    

    const handleIncrement = () =>{
        console.log(count.current = count.current+1);
    }




    return (
      <div>
        <input type='text' placeholder='Name' onChange={(e)=> {setName(e.target.value)}} ref={username}/>
        <input type='text' placeholder='email'  ref={useremail} />
        <button onClick={handleFocus}>submit</button>
        <button onclick={handleIncrement}>Increment</button>

      </div>
    )
}

export default UseRef;

    
