import React  from 'react'

function Cardcomponent(props){
    return(
        <div class="card">
            <img src={props.img} alt="image" class="card-img"></img>
            <div class="card-body">
                <h2 class="card-title">{props.title}</h2>
                <p class="card-description">{props.description}</p>
            
            
           
            </div>
 
        </div>
    )
}
export default Cardcomponent

