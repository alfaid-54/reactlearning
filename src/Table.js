import React from 'react';

function Table(props){
  const {data} = props;



  
    return (
      <div>
        <table class='table'>
            <thead >
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Batch</th>
                    <th>Performance</th>
                </tr>
            </thead>
            <tbody>
                {data.map((item) => (
                  <tr key={item}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.batch}</td>
                    <td>{item.performance}</td>
                  </tr>
                ))}

            </tbody>
        </table>
      </div>
    );
    
                }
export default Table;