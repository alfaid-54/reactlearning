import React, { Component } from 'react'

export  default class TableComponent extends Component {
    constructor(props){
        super(props);
  
    this.state = {
        participantdetails: [
            {
                id: 101,
                name: 'Alfaid',
                batch: 54,
                performance: 'good',
                skills:['python','java']
            },
            {
                id: 102,
                name: 'Pranay',
                batch: 54,
                performance: 'good'
            },
            { id: 103, name: 'Lakshmipriya', batch: 54, performance: 'good'},
            { id: 104, name: 'Sresta', batch: 54, performance: 'good'},
            { id: 105, name: 'Karunya', batch: 54, performance: 'good'},
            { id: 106, name: 'Jasmitha', batch: 54, performance: 'good'},
            { id: 107, name: 'Akhila', batch: 54, performance: 'good'},
            { id: 108, name: 'Hanipriya', batch: 54, performance: 'good'},
        ] 
    }
}

  render() {
    return (
      <div>
        <table class='table'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Batch</th>
                    <th>Skills</th>
                    <th>Performance</th>
                    

                </tr>
            </thead>
            <tbody>
                {this.state.participantdetails.map((participant, index) =>(
                    <tr key={index}>
                        <td>{participant.id}</td>
                        <td>{participant.name}</td>
                        <td>{participant.batch}</td>
                        <td>{participant.skills}</td>
                        <td>{participant.performance}</td>

                    </tr>

                ))

                
  }
            </tbody>
        </table>
      </div>
    )
  }
}
